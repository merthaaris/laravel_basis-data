<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PegawaiController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//route CRUD
Route::get('/',[PegawaiController:: class, 'index']);
Route::get('/tambah',[PegawaiController:: class, 'tambah']);
Route::post('/store',[PegawaiController:: class, 'store']);
Route::get('/edit/{id}',[PegawaiController:: class, 'edit']);
Route::post('/update',[PegawaiController:: class, 'update']);
Route::get('/hapus/{id}',[PegawaiController:: class, 'hapus']);